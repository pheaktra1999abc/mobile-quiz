package com.example.mobilequizs.fragment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.mobilequizs.R


class LoginForm : AppCompatActivity() {
    private lateinit var txtID : EditText
    private lateinit var txtPassword : EditText
    private lateinit var btnLogin : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_form)
        initView()
        btnLogin.setOnClickListener{
            if(txtID.text.contains("011223344") && txtPassword.text.contains("123@abc")){
                val intent = Intent(this, MainActivityInfo::class.java)
                startActivity(intent)
                finish()
            }else{
                Toast.makeText(this,"Faled",Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun initView(){
        txtID = findViewById(R.id.txtPhoneNumber)
        txtPassword = findViewById(R.id.txtPassword)
        btnLogin = findViewById(R.id.btnLoginFragment)
    }

}
