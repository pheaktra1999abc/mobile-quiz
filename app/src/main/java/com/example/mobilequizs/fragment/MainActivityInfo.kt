package com.example.mobilequizs.fragment

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.mobilequizs.R
import com.example.mobilequizs.model.NewFeedModel
import com.google.android.material.tabs.TabLayout


class MainActivityInfo : AppCompatActivity() {
    lateinit var pageView: ViewPager
    lateinit var tabLayout: TabLayout
    lateinit var textNameTeacher : TextView
    private var isSelectFirst : Boolean = true;
    private lateinit var newFeedArrayList : ArrayList<NewFeedModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_info)
        pageView = findViewById(R.id.viewPageInfo)
        tabLayout = findViewById(R.id.tabLayoutInfo)
        tabLayout.addTab(tabLayout.newTab().setText("My Student"))
        tabLayout.addTab(tabLayout.newTab().setText("Sharing"))
        tabLayout.addTab(tabLayout.newTab().setText("Post"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MyAdapter(this, supportFragmentManager, tabLayout.tabCount)
        pageView.adapter = adapter
        pageView.setCurrentItem(1);
        //binding = ActivityAfterLoginBinding.inflate(layoutInflater)


        pageView.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(tabLayout)
        )
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pageView.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                if (isSelectFirst){
                    pageView!!.currentItem = 0
                    isSelectFirst = false
                }
            }
        })

    }

    class MyAdapter(
        private val myContext: Context,
        fm: FragmentManager,
        internal var totalTabs: Int
    ) : FragmentPagerAdapter(fm) {


        // this is for fragment tabs
        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> {
                    return MyStudentFragment()
                }
                1 -> {
                    return SharingFragment()
                }
                2 -> {
                    return PostFragment()
                }
                else -> return null!!
            }
        }

        // this counts total number of tabs
        override fun getCount(): Int {
            return totalTabs
        }


    }

    }