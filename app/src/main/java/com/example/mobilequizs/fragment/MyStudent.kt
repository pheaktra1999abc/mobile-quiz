package com.example.mobilequizs.fragment
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilequiz.helper.SQLiteHelper
import com.example.mobilequiz.models.StudentModels
import com.example.mobilequizs.R

class MyStudentFragment : Fragment(){
    private lateinit var sqlHelper: SQLiteHelper
    private lateinit var tableRecyclerView: RecyclerView
    private lateinit var tableRowAdapter: TableRowAdapter
    private lateinit var btnInsertStudent: Button
    override fun onResume() {
        super.onResume()
        getAllStduent()
        tableRowAdapter.notifyDataSetChanged()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sqlHelper = context?.let { SQLiteHelper(it) }!!
        val view = inflater.inflate(R.layout.students_info, container, false)
        tableRecyclerView = view!!.findViewById(R.id.recyclerView)
        initRecycle()
        getAllStduent()
        tableRowAdapter.setOnClickDeleteItems {
            deleteStudentById(it.StudentId)
        }
        tableRowAdapter.setOnClickUpdateItems {
            updateStudentById(StudentModels(it.StudentId,it.studentName,it.studentSex,it.StudentSubject))

        }
        btnInsertStudent = view.findViewById(R.id.btnCreateStudent)
        btnInsertStudent.setOnClickListener {
            var intent =  Intent(context, MainActivityInsert::class.java)
            intent.putExtra("valueBtn","Create")
            requireActivity().run {
                startActivity(
                    intent,
                )
            }
        }
        return view
    }

    class TableRowAdapter() :
        RecyclerView.Adapter<TableRowAdapter.ViewHolder>() {
        private var onClickDeleteItem: ((StudentModels) -> Unit)? = null
        private var onClickUpdateItem: ((StudentModels) -> Unit)? = null
        private var stdList: ArrayList<StudentModels> = ArrayList()
        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

            val v: View = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.table_row_layou1, viewGroup, false)


            return ViewHolder(v)
        }

        fun addStudentItem(items: ArrayList<StudentModels>) {
            this.stdList = items
        }

        fun setOnClickUpdateItems(callback: (StudentModels) -> Unit) {
            this.onClickUpdateItem = callback
        }

        fun setOnClickDeleteItems(callback: (StudentModels) -> Unit) {
            this.onClickDeleteItem = callback
        }

        override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
            val std = stdList[i]
            viewHolder.txtIDStudent.text = stdList[i].StudentId.toString()
            viewHolder.txtNameStudent.text = stdList[i].studentName
            viewHolder.txtSexStudent.text = stdList[i].studentSex
            viewHolder.txtSubjectStudent.text = stdList[i].StudentSubject
            viewHolder.btnDeletedStudent.setOnClickListener { this.onClickDeleteItem?.invoke(std) }
            viewHolder.btnEditStudent.setOnClickListener { this.onClickUpdateItem?.invoke(std) }
        }

        override fun getItemCount(): Int {
            return stdList.size
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val txtIDStudent: TextView = itemView.findViewById(R.id.txtIDStudent)
            val txtNameStudent: TextView = itemView.findViewById(R.id.txtNameStudent)
            val txtSexStudent: TextView = itemView.findViewById(R.id.txtSexStudent)
            val txtSubjectStudent: TextView = itemView.findViewById(R.id.txtSubject)
            val btnEditStudent: Button = itemView.findViewById(R.id.btnEditStudent)
            val btnDeletedStudent: Button = itemView.findViewById(R.id.btnDeleteStudent)
        }

    }


    private fun updateStudentById(stu : StudentModels) {
        println("Update")
        if (stu.StudentId == null) return
        var intent =  Intent(context, MainActivityInsert::class.java)
        intent.putExtra("btnValue","Update")
        intent.putExtra("idStudent",stu.StudentId)
        intent.putExtra("nameStudent",stu.studentName)
        intent.putExtra("sexStudent",stu.studentSex)
        intent.putExtra("subjectStudent",stu.StudentSubject)

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        requireActivity().run {
            startActivity(
                intent,
            )
        }


    }

    private fun deleteStudentById(id: Int) {
        println("DFDSF $id")
        if (id == null) return
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Do you want to delete this record?")
        builder.setCancelable(true)
        builder.setPositiveButton("Yes") { dialog, _ ->
            sqlHelper.deleteStudentById(id)
            getAllStduent()
            tableRowAdapter.notifyDataSetChanged()
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog, _ ->
            dialog.dismiss()
        }
        var alert = builder.create()
        alert.show()
    }

    private fun initRecycle() {
        tableRowAdapter = TableRowAdapter()
        tableRecyclerView.layoutManager = LinearLayoutManager(context)
        tableRecyclerView.adapter = tableRowAdapter

    }

    private fun getAllStduent() {
        var stuList = sqlHelper.getAllStudent()

        println("hehe ${stuList.size}")
        tableRowAdapter.addStudentItem(stuList)
    }

}
