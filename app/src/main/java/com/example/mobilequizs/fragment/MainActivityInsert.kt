package com.example.mobilequizs.fragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilequiz.helper.SQLiteHelper
import com.example.mobilequiz.models.StudentModels
import com.example.mobilequizs.R

class MainActivityInsert : AppCompatActivity() {
    private lateinit var txtID: EditText
    private lateinit var txtName: EditText
    private lateinit var txtSex: EditText
    private lateinit var txtSubject: EditText
    private lateinit var btnInsertStudent: Button
    private lateinit var sqlHelper: SQLiteHelper
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_insert)
        initView()
        val bundle : Bundle =  intent.extras as Bundle
        val valueBtn = bundle.get("btnValue")
        val idStudent = bundle.get("idStudent")
        val nameStudent = bundle.get("nameStudent")
        val sexStudent = bundle.get("sexStudent")
        val subjectStudent = bundle.get("subjectStudent")

        if(valueBtn == "Update"){
            btnInsertStudent.text = valueBtn.toString()
            txtName.setText(nameStudent.toString())
            txtSex.setText(sexStudent.toString())
            txtSubject.setText(subjectStudent.toString())
        }
        sqlHelper = SQLiteHelper(this)
        btnInsertStudent.setOnClickListener {
            if(valueBtn == "Update"){
                updateStudent(idStudent as Int)
            }else
                addStduent()
        }


    }

    private fun initView() {
        txtName = findViewById(R.id.studentNameInsert)
        txtSex = findViewById(R.id.studentSexInsert)
        txtSubject = findViewById(R.id.studentSubjectInsert)
        btnInsertStudent = findViewById(R.id.btnCreate)
    }

    private fun addStduent() {
        val name = txtName.text.toString()
        val sex = txtSex.text.toString()
        val sub = txtSubject.text.toString()
        if ( name.isEmpty() || sex.isEmpty() || sub.isEmpty()) {
            Toast.makeText(this, "Please Enter Required Field", Toast.LENGTH_SHORT).show()
        } else {
            val stu = StudentModels(getLastRecordStudent()+1, name, sex, sub)
            val status = sqlHelper.insertStudent(stu)
            if (status > -1) {
                Toast.makeText(this, "Insert Success", Toast.LENGTH_SHORT).show()
                println("Success")
                ClearText()
                val handler =  Handler()
                handler.postDelayed({finish()},1500)
            } else {
                Toast.makeText(this, "Record Not Saved", Toast.LENGTH_SHORT).show()
                println("Failed")
            }

        }
    }
    private fun updateStudent(idStud : Int){
        val id = idStud
        val nameUpdate = txtName.text.toString()
        val sexUpdate = txtSex.text.toString()
        val subUpdate = txtSubject.text.toString()
        if (id==null || nameUpdate.isEmpty() || sexUpdate.isEmpty() || subUpdate.isEmpty()) {
            Toast.makeText(this, "Record Not Changed", Toast.LENGTH_SHORT).show()
        } else {
            val stu = StudentModels(id, nameUpdate, sexUpdate, subUpdate)
            val status = sqlHelper.updateStudent(stu)
            if (status > -1) {
                Toast.makeText(this, "Update Success", Toast.LENGTH_SHORT).show()
                ClearText()
                val handler =  Handler()
                handler.postDelayed({finish()},1500)
            } else {
                Toast.makeText(this, "Update Failed", Toast.LENGTH_SHORT).show()
                println("Failed")
            }

        }
    }

    fun getLastRecordStudent(): Int {
        var getId = sqlHelper.getLastId()
        return getId.StudentId
    }

    fun ClearText() {

        txtName.setText("")
        txtSex.setText("")
        txtSubject.setText("")

    }

}