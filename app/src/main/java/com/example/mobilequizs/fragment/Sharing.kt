package com.example.mobilequizs.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.mobilequizs.model.NewFeedModel
import com.example.mobilequizs.R
import com.example.mobilequizs.constraint.SharingList


class SharingFragment : Fragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//
        val view=inflater.inflate(R.layout.fragment_sharing, container, false)
        val listView = view!!.findViewById<ListView>(R.id.listViewSharing)
        var list = mutableListOf<NewFeedModel>()
        var sharingList : SharingList = SharingList()
//        newFeedArrayList = ArrayList();
        for(i in sharingList.nameProfile.indices){
            var newFeed = NewFeedModel(sharingList.imageProfileId[i], sharingList.nameProfile[i],sharingList.status[i],sharingList.imagePostId[i],sharingList.timeAgo[i])
//            newFeedArrayList.add(newFeed)
            list.add(newFeed)
            println(newFeed.status)
        }

        val myListAdapter = MyAdapterNewFeed(container!!.context,R.layout.list_item,list)
        listView.adapter = myListAdapter


        return view
    }

}

