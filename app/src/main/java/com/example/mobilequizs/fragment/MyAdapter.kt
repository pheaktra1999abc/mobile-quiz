package com.example.mobilequizs.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.mobilequizs.R
import com.example.mobilequizs.model.NewFeedModel

class MyAdapterNewFeed( context: Context,var resourse : Int, private val arrayList: List<NewFeedModel>) :
    ArrayAdapter<NewFeedModel>(
        context,resourse,arrayList
    ) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater : LayoutInflater = LayoutInflater.from(context)
        val view : View = inflater.inflate(resourse,null)
        val profilePhoto : ImageView = view.findViewById(R.id.imageProfile)
        val timeAfterPost : TextView = view.findViewById(R.id.timeAgo)
        val nameProfile : TextView = view.findViewById(R.id.nameProfile)
        val photoPost : ImageView = view.findViewById(R.id.imagePosts)
        val statusPost : TextView = view.findViewById(R.id.status)

        profilePhoto.setImageDrawable(context.resources.getDrawable(arrayList[position].photoNewFeed))
        timeAfterPost.text = arrayList[position].timeafterPost
        nameProfile.text = arrayList[position].nameProfile
        photoPost.setImageDrawable(context.resources.getDrawable(arrayList[position].photoNewFeed))
        statusPost.text = arrayList[position].status

        return view
    }

    }