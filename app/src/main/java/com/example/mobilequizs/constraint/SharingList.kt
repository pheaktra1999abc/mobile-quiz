package com.example.mobilequizs.constraint

import com.example.mobilequizs.R

class SharingList {
    var imageProfileId = intArrayOf(
        R.drawable.image1,
        R.drawable.image2,
        R.drawable.image3,
        R.drawable.image4,
        R.drawable.image5,
        R.drawable.image6,
        R.drawable.image7,
        R.drawable.image8,
        R.drawable.image1,
    )
    var imagePostId = intArrayOf(
        R.drawable.image1,
        R.drawable.image2,
        R.drawable.image3,
        R.drawable.image4,
        R.drawable.image5,
        R.drawable.image6,
        R.drawable.image7,
        R.drawable.image8,
        R.drawable.image1,
    )
    var nameProfile = arrayOf(
        "Dara",
        "Thona",
        "KaVana_kh",
        "Rithy_Fc",
        "Pheaktra_kh",
        "Khmeng_Khmer",
        "Dara_Rothana",
        "veaSna",
        "Reak_Sa_Btd",
    )
    var timeAgo = arrayOf(
        "10h",
        "1h",
        "12h",
        "3h",
        "33mine",
        "2h",
        "10mine",
        "10h",
        "7h",
    )
    var status = arrayOf(
        "love you so much my dear",
        "Are you ok ",
        "Nice place in kp",
        "I wanna go everywhere with my family",
        "Hey you, Are you single",
        "Hello from me",
        "I locv you so much",
        "Hey girl where are you ?",
        "I like you .......",
    )
}