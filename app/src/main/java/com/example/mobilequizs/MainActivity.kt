package com.example.mobilequizs

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ListView
import com.example.mobilequizs.constraint.SharingList
import com.example.mobilequizs.fragment.MyAdapterNewFeed
import com.example.mobilequizs.fragment.LoginForm
import com.example.mobilequizs.model.NewFeedModel

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)

        val listView = findViewById<ListView>(R.id.listViewMainActivity)
        var list = mutableListOf<NewFeedModel>()
        var shareItem: SharingList = SharingList()

        //-- use for add record to display next time  -/
        for (i in shareItem.nameProfile.indices) {
            var newFeed = NewFeedModel(
                shareItem.imageProfileId[i],
                shareItem.nameProfile[i],
                shareItem.status[i],
                shareItem.imagePostId[i],
                shareItem.timeAgo[i]
            )
            list.add(newFeed)
            println(newFeed.status)
        }

        val myListAdapter = MyAdapterNewFeed(this, R.layout.list_item, list)
        listView.adapter = myListAdapter
        val button: Button = findViewById(R.id.btnlogin)
        //-- Use for Route to LoginForm page -/
        button.setOnClickListener {
            val intent = Intent(this, LoginForm::class.java)
            startActivity(intent)
            finish()
        }
    }



}




