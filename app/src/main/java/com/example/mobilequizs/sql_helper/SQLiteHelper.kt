package com.example.mobilequiz.helper

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilequiz.models.StudentModels
import com.example.mobilequizs.R

class SQLiteHelper(context: Context) : SQLiteOpenHelper(context,DATABASE_NAME, null,DATABASE_VERSION) {
    private lateinit var sqlHelper: SQLiteHelper
    private lateinit var tableRecyclerView: RecyclerView
    private lateinit var tableRowAdapter: TableRowAdapter
    private lateinit var btnInsertStudent: Button

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "student.db"
        private const val TBL_STUDENT = "tbl_student"
        private const val ID = "id"
        private const val NAME = "name"
        private const val SEX = "sex"
        private const val SUBJECT = "subject"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTableStudent =
            ("CREATE TABLE " + TBL_STUDENT + "(" + ID + " INTEGER PRIMARY KEY," + NAME + " TEXT," + SEX + " TEXT," + SUBJECT + " TEXT)")
        db?.execSQL(createTableStudent)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TBL_STUDENT")
        onCreate(db)
    }


    fun insertStudent(stu: StudentModels): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID, stu.StudentId)
        contentValues.put(NAME, stu.studentName)
        contentValues.put(SEX, stu.studentSex)
        contentValues.put(SUBJECT, stu.StudentSubject)
        val success = db.insert(TBL_STUDENT,null,contentValues)
        db.close()
        return success
    }
    @SuppressLint("Range")
    fun getAllStudent() :ArrayList<StudentModels>{
        val stdList: ArrayList<StudentModels> = ArrayList()
        val selectQuery = "SELECT * FROM $TBL_STUDENT"
        val  db = this.readableDatabase
        val cursor : Cursor?
        try{
            cursor = db.rawQuery(selectQuery,null)
        }catch (e:Exception){
            e.printStackTrace()
            db.execSQL(selectQuery)
            return  arrayListOf()
        }
        var id : Int
        var nameStudent : String
        var sexStudent :String
        var subjectStudent :String
        if(cursor.moveToFirst()){
            do {
                id = cursor.getInt(cursor.getColumnIndex("$ID"))
                nameStudent = cursor.getString(cursor.getColumnIndex("$NAME"))
                sexStudent = cursor.getString(cursor.getColumnIndex("$SEX"))
                subjectStudent = cursor.getString(cursor.getColumnIndex("$SUBJECT"))

                val std = StudentModels(id,nameStudent,sexStudent,subjectStudent)
                stdList.add(std)
            }while (cursor.moveToNext())
        }
        return stdList
    }
    fun deleteStudentById(id:Int):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID,id)
        val success = db.delete(TBL_STUDENT,"id=$id",null)
        db.close()
        return success
    }
    fun updateStudent(stu:StudentModels) :Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID, stu.StudentId)
        contentValues.put(NAME, stu.studentName)
        contentValues.put(SEX, stu.studentSex)
        contentValues.put(SUBJECT, stu.StudentSubject)
        val success = db.update(TBL_STUDENT,contentValues,"id=" + stu.StudentId,null)
        db.close()
        return success
    }
    private fun deleteStudentById(id: Int,context: Context) {
        println("DFDSF $id")
        if (id == null) return
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Do you want to delete this record?")
        builder.setCancelable(true)
        builder.setPositiveButton("Yes") { dialog, _ ->
            sqlHelper.deleteStudentById(id)
            getAllStduent()
            tableRowAdapter.notifyDataSetChanged()
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog, _ ->
            dialog.dismiss()
        }
        var alert = builder.create()
        alert.show()
    }
    private fun getAllStduent() {
        var stuList = sqlHelper.getAllStudent()

        println("hehe ${stuList.size}")
        tableRowAdapter?.addStudentItem(stuList)
    }

    @SuppressLint("Range")
    fun getLastId() :StudentModels{
        var std:StudentModels = StudentModels()
        val selectQuery = "SELECT * FROM $TBL_STUDENT WHERE $ID = (SELECT MAX($ID) FROM $TBL_STUDENT) "
        val  db = this.readableDatabase
        val cursor : Cursor?
        try{
            cursor = db.rawQuery(selectQuery,null)
        }catch (e:Exception){
            e.printStackTrace()
            db.execSQL(selectQuery)
            return  StudentModels()
        }
        var id : Int
        var nameStudent : String
        var sexStudent :String
        var subjectStudent :String
        if(cursor.moveToFirst()){
            do {
                id = cursor.getInt(cursor.getColumnIndex("$ID"))
                nameStudent = cursor.getString(cursor.getColumnIndex("$NAME"))
                sexStudent = cursor.getString(cursor.getColumnIndex("$SEX"))
                subjectStudent = cursor.getString(cursor.getColumnIndex("$SUBJECT"))

                std = StudentModels(id,nameStudent,sexStudent,subjectStudent)

            }while (cursor.moveToNext())
        }
        return std
    }





}
class TableRowAdapter() : RecyclerView.Adapter<TableRowAdapter.ViewHolder>() {
    private var onClickDeleteItem: ((StudentModels) -> Unit)? = null
    private var onClickUpdateItem: ((StudentModels) -> Unit)? = null
    private var stdList: ArrayList<StudentModels> = ArrayList()
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

        val v: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.students_info, viewGroup, false)


        return ViewHolder(v)
    }

    fun addStudentItem(items: ArrayList<StudentModels>) {
        this.stdList = items
    }

    fun setOnClickUpdateItems(callback: (StudentModels) -> Unit) {
        this.onClickUpdateItem = callback
    }

    fun setOnClickDeleteItems(callback: (StudentModels) -> Unit) {
        this.onClickDeleteItem = callback
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val std = stdList[i]
        viewHolder.txtIDStudent.text = stdList[i].StudentId.toString()
        viewHolder.txtNameStudent.text = stdList[i].studentName
        viewHolder.txtSexStudent.text = stdList[i].studentSex
        viewHolder.txtSubjectStudent.text = stdList[i].StudentSubject
        viewHolder.btnDeletedStudent.setOnClickListener { this.onClickDeleteItem?.invoke(std) }
        viewHolder.btnEditStudent.setOnClickListener { this.onClickUpdateItem?.invoke(std) }
    }

    override fun getItemCount(): Int {
        return stdList.size
    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtIDStudent: TextView = itemView.findViewById(R.id.txtIDStudent)
        val txtNameStudent: TextView = itemView.findViewById(R.id.txtNameStudent)
        val txtSexStudent: TextView = itemView.findViewById(R.id.txtSexStudent)
        val txtSubjectStudent: TextView = itemView.findViewById(R.id.txtSubject)
        val btnEditStudent: Button = itemView.findViewById(R.id.btnEditStudent)
        val btnDeletedStudent: Button = itemView.findViewById(R.id.btnDeleteStudent)
    }


}


